---
title: Venue
slug: venue
description: null
draft: false
menu:
  main:
    name: Venue
    weight: 999
url: /venue/
---

Le SED du centre Inria de l'Université de Rennes va accueillir 
les ingénieurs qui participeront à ce premier rencontre.

## Comment venir ?

### Adresse :

Campus universitaire de Beaulieu - Avenue du Général Leclerc 35042 Rennes Cedex

[Plan d’accès sur Google Map](https://www.google.fr/maps/place/Inria+de+l'Universit%C3%A9+de+Rennes/@48.1162479,-1.6394422,17z/data=!3m1!4b1!4m5!3m4!1s0x480edee5a599f107:0x318da7854b094389!8m2!3d48.1162039!4d-1.6396323)

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2663.6883211328964!2d-1.6394422000000002!3d48.116247900000005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x480edee5a599f107%3A0x318da7854b094389!2sInria%20de%20l&#39;Universit%C3%A9%20de%20Rennes!5e0!3m2!1sfr!2sfr!4v1666873590150!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

### Depuis la gare SNCF :

Prendre le Métro a à la station Gares (Rennes) en direction de J.F. Kennedy (Rennes) Descendre à la station « République » A la station République, prendre la ligne C4 en direction de ZA Saint-Sulpice ou la ligne C6 vers Cesson Sévigné/Chantepie Descendre à l’arrêt « Tournebride ».

Plus des [infos ici](https://www.inria.fr/fr/comment-venir-au-centre-inria-de-luniversite-de-rennes)
