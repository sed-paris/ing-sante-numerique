# Réseaux thématique de développement logiciel en Santé Numèrique Inria

Ce dépôt integre l'information generée dans le réseaux thématique autour de ce sujet.

Les sites web pour les differentes rencontres sont disponibles dans Gitlab pages :


- [1er rencontre Rennes, décembre 2022](http://sed-paris.gitlabpages.inria.fr/ing-sante-numerique/2022/)
- 2ème rencontre Bordeaux, mars 2023
- [3éme rencontre Rennes, septembre 2023](http://sed-paris.gitlabpages.inria.fr/ing-sante-numerique/2023/)


